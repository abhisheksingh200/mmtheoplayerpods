//
//  TheoMMIntegration-iOS.swift
//  TheoiOSPlayer
//
//  Created by Rupesh on 4/3/19.
//  Copyright © 2019 MediaMelon. All rights reserved.
//

import Foundation
import THEOplayerSDK
import CoreTelephony


//public class MMAVAssetInformation : Codable{
//    /*
//     * Creates an object to hold information identifying the asset that is to be played back on the AVPlayer
//     * User must specify URL of the asset.
//     * User may optionally specify the identifier identifying the asset, its name and collection to which this asset belongs
//     */
//    public init(assetURL aURL:String, assetID aId:String?, assetName aName:String?, videoId vId:String?){
//        assetURL = aURL
//
//        if let aId = aId{
//            assetID = aId
//        }
//
//        if let aName = aName{
//            assetName = aName
//        }
//
//        if let vId = vId{
//            videoId = vId
//        }
//
//        customKVPs = [:]
//        overridableMetrics = [:]
//    }
//
//    /*
//     * Lets user specify the custom metadata to be associated with the asset, for example - Genre, DRM etc
//     *
//     * Call to this API is optional
//     */
//    public func addCustomKVP(_ key:String, _ value:String){
//        customKVPs[key] = value
//    }
//
//    /*
//     * Lets user specify the value for overridable metrics, like CDN, Latency etc.
//     *
//     * Call to this API is optional
//     */
//    public func addMMOverridableMetricValue(_ metric:MMOverridableMetrics, _ value:String){
////        overridableMetrics[metric.rawValue] = value
//    }
//
//    /*
//     * Sets the mode to be used for QBR and the meta file url from where content metadata can be had.
//     * Meta file URL is to be provided only if metadata cant be had on the default location.
//     *
//     * Please note that call to this method is needed only if QBR is integrated to the player.
//     */
//    public func setQBRMode(_ mode:String, withMetaURL metaURL:URL?){ //Needed only when QBR is to be integrated
//        qbrMode = mode
//        if let metaURL = metaURL{
//            metafileURL = metaURL
//        }
//    }
//
//    public static let KQBRModeQuality = "QBRModeQuality"
//    public static let KQBRModeBitsave = "QBRModeBitsave"
//    public static let KQBRModeDisabled = "QBRModeDisabled"
//    public var qbrMode:String? //Needed only when QBR is to be integrated
//    public var metafileURL:URL? //Needed only when QBR is to be integrated
//
//    public var assetURL:String //URL of the Asset
//    public var assetID:String? //optional identifier of the asset
//    public var assetName:String? //optional name of the asset
//    public var videoId:String? //optional identifier of the asset group (or) sub asset
//    public var customKVPs:[String:String] //Custom Metadata of the asset
//    public var overridableMetrics:[String:String] //Overridable Metrics
//}



class ReachabilityManager: NSObject {
    public static  let shared = ReachabilityManager()
    let reachability = ReachabilityMM()!
    var reachabilityStatus: ReachabilityMM.NetworkStatus = .notReachable
    
    @objc func reachabilityChanged(notification: Notification) {
        let reachability = notification.object as! ReachabilityMM
        
        var connInfo:MMConnectionInfo = .notReachable
        switch reachability.currentReachabilityStatus {
        case .notReachable:
            connInfo = .notReachable
        case .reachableViaWiFi:
            connInfo = .wifi
        case .reachableViaWWAN:
            connInfo = .cellular
        }
        
        TheoMMIntegration.shared.reportNetworkType(connInfo:connInfo)
    }
    
    func startMonitoring() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.reachabilityChanged),
                                               name: ReachabilityChangedNotificationMM,
                                               object: reachability)
        do{
            try reachability.startNotifier()
        } catch {
        }
    }
    
    func stopMonitoring(){
        reachability.stopNotifier()
        NotificationCenter.default.removeObserver(self,
                                                  name: ReachabilityChangedNotificationMM,
                                                  object: reachability)
    }
}

public class TheoMMIntegration:NSObject, MMSmartStreamingObserver{
    
    /*
     * Singleton instance of the adaptor
     */
    public static  let shared = TheoMMIntegration()
    
    /*
     * Key for specifying asset information in the Metadata of SourceDescription of TheoPlayer
     */
    public static let KAssetInformationKey = "MMAssetInformation"
    
    /**
     * Gets the version of the SDK
     */
    public static func getVersion()->String!{
        return MMSmartStreaming.getVersion()
    }
    
    /**
     * If for some reasons, accessing the content manifest by SDK interferes with the playback. Then user can disable the manifest fetch by the SDK.
     * For example - If there is some token scheme in content URL, that makes content to be accessed only once, then user of SDK may will like to call this API.
     * So that player can fetch the manifest
     */
    public static func disableManifestsFetch(disable:Bool){
        return MMSmartStreaming.disableManifestsFetch(disable)
    }
    
    /**
     * Allows user of SDK to provide information on Customer, Subscriber and Player to the SDK
     * Please note that it is sufficient to call this API only once for the lifetime of the application, and all playback sessions will reuse this information.
     *
     * Note - User may opt to call initializeAssetForPlayer instead of calling this API, and provide the registration information in its param every time as they provide the asset info. This might help ease the integration.
     *
     * This API doesnt involve any network IO, and is very light weight. So calling it multiple times is not expensive
     */
    public static func setPlayerRegistrationInformation(registrationInformation pInfo:MMRegistrationInformation?, player aPlayer:THEOplayer?){
        if let pInfo = pInfo{
            TheoMMIntegration.logDebugStatement("=============setPlayerInformation - pInfo=============")
            TheoMMIntegration.registerMMSmartStreaming(playerName: pInfo.playerName, custID: pInfo.customerID, component: pInfo.component ?? "THEOSDK", subscriberID: pInfo.subscriberID, domainName: pInfo.domainName, subscriberType: pInfo.subscriberType, subscriberTag: pInfo.subscriberTag)
            TheoMMIntegration.reportPlayerInfo(brand: pInfo.playerBrand, model: pInfo.playerModel, version: pInfo.playerVersion)
        }
        if let oldPlayer = TheoMMIntegration.shared.player{
            TheoMMIntegration.shared.cleanupSession(player: oldPlayer)
        }
        if let newPlayer = aPlayer{
            TheoMMIntegration.shared.createSession(player: newPlayer)
        }
    }
    
    /**
     * Whenever the asset with the player is changed, user of the API may call this API
     * Please note either changeAssetForPlayer or initializeAssetForPlayer should be called
     */
    private static func changeAssetForPlayer(assetInfo aInfo:MMAssetInformation){
        TheoMMIntegration.logDebugStatement("================changeAssetForPlayer \(aInfo.assetURL)=============")
        TheoMMIntegration.shared.cleanupCurrItem();
        TheoMMIntegration.shared.assetInfo = aInfo
        TheoMMIntegration.shared.initSession(deep: true)
    }
    
    /**
     * Once the application is done with the player, it should call this API to remove the observors set on player before cleaning up player
     */
    public static func cleanUp(){
        TheoMMIntegration.logDebugStatement("================cleanUp=============")
        shared.cleanupInstance()
    }
    
    /**
     * Application may update the subscriber information once it is set via MMRegistrationInformation
     */
    public static func updateSubscriber(subscriberId:String!, subscriberType:String!, subscriberMetadata:String!){
        MMSmartStreaming.updateSubscriber(withID: subscriberId, andType: subscriberType, withTag:subscriberMetadata)
    }
    
    /**
     * Application may report the custom metadata associated with the content using this API.
     * Application can set it as a part of MMAssetInformation before the start of playback, and
     * can use this API to set metadata during the course of the playback.
     */
    public func reportCustomMetadata(key:String!, value:String!){
        mmSmartStreaming.reportCustomMetadata(withKey: key, andValue: value)
    }
    
    /**
     * Used for debugging purposes, to enable the log trace
     */
    public func enableLogTrace(logStTrace:Bool){
        TheoMMIntegration.enableLogging = logStTrace
        mmSmartStreaming.enableLogTrace(logStTrace)
    }
    
    /**
     * If application wants to send application specific error information to SDK, the application can use this API.
     * Note - SDK internally takes care of notifying the error messages provided to it by AVFoundation framwork
     */
    public func reportError(error:String, playbackPosMilliSec:Int){
        mmSmartStreaming.reportError(error, atPosition: playbackPosMilliSec)
    }
    
    /**
     * Lets the application override the value of a metric
     */
    public func reportMetricValue(metricToOverride:MMOverridableMetrics, value:String!){
        switch metricToOverride {
        case MMOverridableMetrics.CDN:
            mmSmartStreaming.reportMetricValue(for: MMOverridableMetric.ServerAddress, value: value)
        case MMOverridableMetrics.Latency:
            mmSmartStreaming.reportMetricValue(for: MMOverridableMetric.Latency, value: value)
        case MMOverridableMetrics.DurationWatched:
            mmSmartStreaming.reportMetricValue(for: MMOverridableMetric.DurationWatched, value: value)
        }
    }
    
    private enum TheoPlayerItemPropertiesToObserve: String{
        case Play = "play", //Thrown when the play method is invoked.
        Playing = "playing", //Thrown when the player starts with the playback of media.
        Pause = "pause", //Thrown when the pause method is invoked.
        Progress = "progress", //Thrown when new media data is added to the buffer of the player.
        DurationChange = "durationChange", //Thrown when the duration of the video changes to a new value. [duration field is used]
        Seeked = "seeked",//Thrown when the player has completed the setting of a new value to the currentTime property.
        Waiting = "waiting", //Thrown when the player has completed the setting of a new value to the currentTime property.
        TimeUpdate = "timeUpdate", //Periodically thrown during playback to indicate that the currentTime property is changing to indicate the new playback position.
        Ended = "ended", //Thrown when the players currentTime property has reached the end of the set source.
        RateChange = "rateChange", //Thrown when the playbackRate property of the player is set to a new value.
        ReadyStateChange = "readyStateChange", //Thrown when the readyState property of the player changes to a new value.
        LoadedMetaData = "loadedMetadata", //Thrown the first time the readyState property goes from 0 – HAVE_NOTHING – to 1 – HAVE_METADATA.
        LoadedData = "loadedData", //Thrown the first time the readyState property goes from 1 – HAVE_METADATA – to 2 – HAVE_CURRENT_DATA.
        CanPlay = "canPlay", //Thrown to indicate that the player can now continue playback, usually after a ‘waiting’ event.
        CanPlayThrough = "canPlayThrough", //Thrown to indicate that the player can now continue playback uninterrupted with a readyState property value of 4 – HAVE_ENOUGH_DATA.
        Error = "error", //Thrown to indicate that an error has occurred in the player.
        PresentationModeChange = "presentationModeChange", //Thrown when the presentationMode has been changed. The new presentationMode can be retrieved via the presentationMode player’s property.
        Resize = "resize", //Thrown to indicate that the playing video has been resize.
        LoadStart = "loadStart", //Thrown the first time the readyState property goes from 1 – HAVE_METADATA – to 2 – HAVE_CURRENT_DATA.
        Destroy = "destroy", //Thrown to indicate that the playing video has been destroyed.
        TrackChangeEvent = "trackChange", //TrackChangeEvent
        SourceChange = "sourceChange"
    }
    
    private static func logDebugStatement(_ logStatement:String){
        if(TheoMMIntegration.enableLogging){
            print(logStatement)
        }
    }
    
    private static func registerMMSmartStreaming(playerName:String, custID:String, component:String,  subscriberID:String?, domainName:String?, subscriberType:String?, subscriberTag:String?){
        MMSmartStreaming.registerForPlayer(withName: playerName, forCustID: custID, component: component, subsID: subscriberID, domainName: domainName, andSubscriberType: subscriberType, withTag:subscriberTag);
        let phoneInfo = CTTelephonyNetworkInfo()
        var operatorName = ""
        if let carrierName = phoneInfo.subscriberCellularProvider?.carrierName{
            operatorName = carrierName
        }
        
        let brand = "Apple"
        let model = UIDevice.current.model
        
        let osName = "iOS"
        let osVersion =  UIDevice.current.systemVersion
        
        let screenWidth = Int(UIScreen.main.bounds.width)
        let screenHeight = Int(UIScreen.main.bounds.height)
        
        MMSmartStreaming.reportDeviceInfo(withBrandName: brand, deviceModel: model, osName: osName, osVersion: osVersion, telOperator: operatorName, screenWidth: screenWidth, screenHeight: screenHeight, andType: model)
    }
    
    private static func reportPlayerInfo(brand:String?, model:String?, version:String?){
        MMSmartStreaming.reportPlayerInfo(withBrandName: brand, model: model, andVersion: version)
    }
    
    private func initializeSession(mode:MMQBRMode!, manifestURL:String!, metaURL:String?, assetID:String?, assetName:String?, videoId:String?){
//        var connectionInfo:MMConnectionInfo = .notReachable
//        let reachability = ReachabilityMM()
//        if let reachability = reachability{
//            do{
//                try reachability.startNotifier()
//                let status = reachability.currentReachabilityStatus
//
//                if(status == .notReachable){
//                    connectionInfo = .notReachable
//                }
//                else if (status == .reachableViaWiFi){
//                    connectionInfo = .wifi
//                }
//                else if (status == .reachableViaWWAN){
//                    connectionInfo = .cellular
//                }
//            }
//            catch{
//
//            }
//        }
//        mmSmartStreaming.reportNetworkType(connectionInfo)
//        mmSmartStreaming.initializeSession(with: mode, forManifest: manifestURL, metaURL: metaURL, assetID: assetID, assetName: assetName, videoID:videoId, for: self)
        
        var connectionInfo:MMConnectionInfo!
        let reachability = ReachabilityMM()

        #if os(iOS)
        func getDetailedMobileNetworkType() {
            let phoneInfo = CTTelephonyNetworkInfo()
            if #available(iOS 12.0, *) {
                if let arrayNetworks = phoneInfo.serviceCurrentRadioAccessTechnology {
                    var networkString = ""
                    for value in arrayNetworks.values {
                        networkString = value
                    }
                    if networkString == CTRadioAccessTechnologyLTE{
                        connectionInfo = .cellular_4G
                    }else if networkString == CTRadioAccessTechnologyWCDMA{
                        connectionInfo = .cellular_3G
                    }else if networkString == CTRadioAccessTechnologyEdge{
                        connectionInfo = .cellular_2G
                    }
                }
            } else {
                let networkString = phoneInfo.currentRadioAccessTechnology
                if networkString == CTRadioAccessTechnologyLTE{
                    connectionInfo = .cellular_4G
                }else if networkString == CTRadioAccessTechnologyWCDMA{
                    connectionInfo = .cellular_3G
                }else if networkString == CTRadioAccessTechnologyEdge{
                    connectionInfo = .cellular_2G
                }
            }
        }
        #endif
        
        if let reachability = reachability{
            do{
                try reachability.startNotifier()
                let status = reachability.currentReachabilityStatus
                
                if(status == .notReachable){
                    connectionInfo = .notReachable
                }
                else if (status == .reachableViaWiFi){
                    connectionInfo = .wifi
                }
                else if (status == .reachableViaWWAN){
                    connectionInfo = .cellular
                    #if os(iOS)
                        getDetailedMobileNetworkType()
                    #endif
                }
            }
            catch{
                
            }
        }
        if (connectionInfo != nil) {
            self.mmSmartStreaming.reportNetworkType(connectionInfo)
        }
        mmSmartStreaming.initializeSession(with: mode, forManifest: manifestURL, metaURL: metaURL, assetID: assetID, assetName: assetName, videoID:videoId, for: self)
    }
    
    func reportNetworkType(connInfo:MMConnectionInfo){
        mmSmartStreaming.reportNetworkType(connInfo)
    }
    
    private func reportLocation(latitude:Double, longitude:Double){
        mmSmartStreaming.reportLocation(withLatitude: latitude, andLongitude: longitude)
    }
    
    public func sessionInitializationCompleted(with status: MMSmartStreamingInitializationStatus, andDescription description: String?, forCmdWithId cmdId: Int) {
        TheoMMIntegration.logDebugStatement("sessionInitializationCompleted - status \(status) description \(String(describing: description))")
    }
    
    private func reportChunkRequest(chunkInfo: MMChunkInformation!){
        mmSmartStreaming.reportChunkRequest(chunkInfo)
    }
    
    private func setPresentationInformation(presentationInfo:MMPresentationInfo!){
        mmSmartStreaming.setPresentationInformation(presentationInfo)
    }
    
    private func reportDownloadRate(downloadRate: Int!){
        mmSmartStreaming.reportDownloadRate(downloadRate)
    }
    
    private func reportBufferingStarted(){
        mmSmartStreaming.reportBufferingStarted()
    }
    
    private func reportBufferingCompleted(){
        mmSmartStreaming.reportBufferingCompleted()
    }
    
    private func reportABRSwitch(prevBitrate:Int, newBitrate:Int){
        mmSmartStreaming.reportABRSwitch(fromBitrate: prevBitrate, toBitrate: newBitrate)
    }
    
    private func reportFrameLoss(lossCnt:Int){
        mmSmartStreaming.reportFrameLoss(lossCnt)
    }
    
    private func logIntegration(str:String){
        
    }
    
    private func reportPresentationSize(width:Int, height:Int){
        mmSmartStreaming.reportPresentationSize(withWidth: width, andHeight: height)
    }
    
//    private func getPlaybackPosition()->Int{
//        guard let player = player else{
//            return 0;
//        }
//
//        //STRANGE : Why would getting current playback pos be async
//        player.requestCurrentTime(completionHandler: {
//            (pos:Double?, err:Error?)->Void in
//            TheoMMIntegration.logDebugStatement("getPlaybackPosition \(String(describing: pos)) \(String(describing: err))")
//        });
//        return 0
//    }
    
    private func cleanupInstance(){
        cleanupCurrItem()
        cleanupSession(player: self.player)
    }
    
    private func cleanupCurrItem(){
        reportStoppedState()
        reset()
    }
    
    private func handleErrorWithMessage(message: String?, error: Error? = nil) {
        TheoMMIntegration.logDebugStatement("--- Error occurred with message: \(String(describing: message)), error: \(String(describing: error)).")
        mmSmartStreaming.reportError(String(describing: error), atPosition: -1)
    }
    
    private func reset(){
        presentationInfoSet = false
        userIntentToPlaySignalled = false
        currentState = CurrentPlayerState.IDLE
        playerDuration = 0
        if TheoMMIntegration.appNotificationObsRegistered == false{
            TheoMMIntegration.appNotificationObsRegistered = true
            
            NotificationCenter.default.addObserver(
                forName: Notification.Name.UIApplicationWillResignActive,
                object: nil,
                queue: nil) { (notification) in
                    ReachabilityManager.shared.stopMonitoring()
            }
            
            NotificationCenter.default.addObserver(
                forName: Notification.Name.UIApplicationDidBecomeActive,
                object: nil,
                queue: nil) { (notification) in
                    ReachabilityManager.shared.startMonitoring()
            }
            ReachabilityManager.shared.startMonitoring()
        }
    }
    
    private func addListeners(){
        guard let player = self.player else {
            return
        }
        if playerContentCallbacksRegd == false{
            self.listeners[TheoPlayerItemPropertiesToObserve.Play.rawValue] = player.addEventListener(type: PlayerEventTypes.PLAY, listener: onPlay)
            self.listeners[TheoPlayerItemPropertiesToObserve.Playing.rawValue] = player.addEventListener(type: PlayerEventTypes.PLAYING, listener: onPlaying)
            self.listeners[TheoPlayerItemPropertiesToObserve.Pause.rawValue] = player.addEventListener(type: PlayerEventTypes.PAUSE, listener: onPause)
            self.listeners[TheoPlayerItemPropertiesToObserve.Progress.rawValue] = player.addEventListener(type: PlayerEventTypes.PROGRESS, listener: onProgress)
            self.listeners[TheoPlayerItemPropertiesToObserve.DurationChange.rawValue] = player.addEventListener(type: PlayerEventTypes.DURATION_CHANGE, listener: onDurationChanged)
            self.listeners[TheoPlayerItemPropertiesToObserve.Seeked.rawValue] = player.addEventListener(type: PlayerEventTypes.SEEKED, listener: onSeeked)
            self.listeners[TheoPlayerItemPropertiesToObserve.Waiting.rawValue] = player.addEventListener(type: PlayerEventTypes.WAITING, listener: onWaiting)
            self.listeners[TheoPlayerItemPropertiesToObserve.TimeUpdate.rawValue] = player.addEventListener(type: PlayerEventTypes.TIME_UPDATE, listener: onTimeUpdate)
            self.listeners[TheoPlayerItemPropertiesToObserve.Ended.rawValue] = player.addEventListener(type: PlayerEventTypes.ENDED, listener: onEnded)
            self.listeners[TheoPlayerItemPropertiesToObserve.Error.rawValue] = player.addEventListener(type: PlayerEventTypes.ERROR, listener: onError)
            self.listeners[TheoPlayerItemPropertiesToObserve.Resize.rawValue] = player.addEventListener(type: PlayerEventTypes.RESIZE, listener: onResize)
            self.listeners[TheoPlayerItemPropertiesToObserve.Destroy.rawValue] = player.addEventListener(type: PlayerEventTypes.DESTROY, listener: onDestroy)
            self.listeners[TheoPlayerItemPropertiesToObserve.SourceChange.rawValue] = player.addEventListener(type:PlayerEventTypes.SOURCE_CHANGE, listener: onSourceChanged)

            self.listeners[TheoPlayerItemPropertiesToObserve.LoadedData.rawValue] = player.addEventListener(type: PlayerEventTypes.LOADED_DATA, listener: onLoadedData)
            self.listeners[TheoPlayerItemPropertiesToObserve.RateChange.rawValue] = player.addEventListener(type: PlayerEventTypes.RATE_CHANGE, listener: onRateChange)
            self.listeners[TheoPlayerItemPropertiesToObserve.ReadyStateChange.rawValue] = player.addEventListener(type: PlayerEventTypes.READY_STATE_CHANGE, listener: onReadyStateChange)
            self.listeners[TheoPlayerItemPropertiesToObserve.LoadedMetaData.rawValue] = player.addEventListener(type: PlayerEventTypes.LOADED_META_DATA, listener: onLoadedMetaData)
            self.listeners[TheoPlayerItemPropertiesToObserve.CanPlay.rawValue] = player.addEventListener(type: PlayerEventTypes.CAN_PLAY, listener: onCanPlay)
            self.listeners[TheoPlayerItemPropertiesToObserve.CanPlayThrough.rawValue] = player.addEventListener(type: PlayerEventTypes.CAN_PLAY_THROUGH, listener: onCanPlayThrough)
            self.listeners[TheoPlayerItemPropertiesToObserve.PresentationModeChange.rawValue] = player.addEventListener(type: PlayerEventTypes.PRESENTATION_MODE_CHANGE, listener: onPresentationModeChange)
            self.listeners[TheoPlayerItemPropertiesToObserve.LoadStart.rawValue] = player.addEventListener(type: PlayerEventTypes.LOAD_START, listener: onLoadStart)
            self.listeners[TheoPlayerItemPropertiesToObserve.TrackChangeEvent.rawValue] = player.videoTracks.addEventListener(type:VideoTrackListEventTypes.CHANGE, listener: onTrackChange)
            playerContentCallbacksRegd = true
        }
    }
    
    private func createSession(player:THEOplayer){
        TheoMMIntegration.logDebugStatement("*** createSession")
        self.player = player
        addListeners()
    }
    
    private func initSession(deep:Bool){
        TheoMMIntegration.logDebugStatement("*** initSession")
        guard let theoPlayer = self.player else{
            return
        }
        
        if(deep){
            reset()
        }
        let serialQueue = DispatchQueue(label: "com.mediamelon.serialq")
//        serialQueue.sync {
            guard let assetInfo = TheoMMIntegration.shared.assetInfo else{
                TheoMMIntegration.logDebugStatement("!!! Error - assetInfo not set !!!")
                return
            }
            
            if(deep){
                var modeToRequest = MMQBRMode.QBRModeDisabled
                
                if assetInfo.qbrMode == MMQBRMode.QBRModeQuality{
                    modeToRequest = MMQBRMode.QBRModeQuality
                }else if assetInfo.qbrMode == MMQBRMode.QBRModeBitsave{
                    modeToRequest = MMQBRMode.QBRModeBitsave
                }
                
                TheoMMIntegration.shared.initializeSession(mode: modeToRequest, manifestURL: assetInfo.assetURL, metaURL: assetInfo.metafileURL? .absoluteString, assetID: assetInfo.assetID, assetName: assetInfo.assetName, videoId: assetInfo.videoId)
                
                for (key, value) in assetInfo.customKVPs{
                    TheoMMIntegration.shared.reportCustomMetadata(key: key, value: value)
                }

//                for (metric, metricValue) in assetInfo.overridableMetrics{
//                    if let metricToSet = MMOverridableMetrics(rawValue: metric){
//                        TheoMMIntegration.shared.reportMetricValue(metricToOverride: metricToSet, value: metricValue)
//                    }
//                }
                
                if(theoPlayer.autoplay == true && userIntentToPlaySignalled == false){
                    TheoMMIntegration.shared.reportUserInitiatedPlayback();
                    userIntentToPlaySignalled = true
                }
               
            }else{
                for (key, value) in assetInfo.customKVPs{
                    TheoMMIntegration.shared.reportCustomMetadata(key: key, value: value)
                }
                
//                for (metric, metricValue) in assetInfo.overridableMetrics{
//                    if let metricToSet = MMOverridableMetrics(rawValue: metric){
//                        TheoMMIntegration.shared.reportMetricValue(metricToOverride: metricToSet, value: metricValue)
//                    }
//                }
            }
            
            addListeners()
            TheoMMIntegration.logDebugStatement("Initializing for \(String(describing: TheoMMIntegration.shared.assetInfo?.assetURL))")
//        }
    }
    
    private func onTrackChange(event:TrackChangeEvent){
        TheoMMIntegration.logDebugStatement("Track Changed event occured \(event)")
    }
    
    private func onAddTrack(event:AddTrackEvent){
        TheoMMIntegration.logDebugStatement("Track Added event occured \(event) \(event.type) \(event.track.uid) ")
        if event.type == "audio"{
            
        }else if event.type == "video"{
            
        }else{
            
        }
    }
    
    private func onPlay(event: PlayEvent) {
        TheoMMIntegration.logDebugStatement("Play event occured \(event)")
        if currentState == .STOPPED{ //replay use case
            if let assetInfo = TheoMMIntegration.shared.assetInfo{
                TheoMMIntegration.changeAssetForPlayer(assetInfo: assetInfo)
            }
        }
        
        if(userIntentToPlaySignalled == false){
            TheoMMIntegration.shared.reportUserInitiatedPlayback();
            userIntentToPlaySignalled = true
        }
        currentState = .PLAY
    }
    
    private func setPresentationInfo(){
        if presentationInfoSet == false{
            guard let theoPlayer = self.player else{
                return
            }
            
            for i in 0..<theoPlayer.videoTracks.count{
                let videoTrack = theoPlayer.videoTracks.get(i)
                print("Video Track \(videoTrack)")
            }
            
            if playerDuration <= 0{
                if let duration = theoPlayer.duration{
                    if duration != Double.nan && duration < Double.infinity{
                        playerDuration = getTimeInMs(timeInSec: duration)
                    }else{
                        playerDuration = -1
                    }
                }
            }
            
            let presInfo = MMPresentationInfo()
            presInfo.isLive = true
            presInfo.duration = playerDuration
            if playerDuration > 0{
                presInfo.isLive = false
            }

            self.setPresentationInformation(presentationInfo: presInfo);
            presentationInfoSet = true
        }
    }
    
    private func onPlaying(event: PlayingEvent) {
        TheoMMIntegration.logDebugStatement("Playing event occured \(event)")
        if presentationInfoSet == false{
            setPresentationSize()
            setPresentationInfo()
        }
        mmSmartStreaming.report(MMPlayerState.PLAYING);
        currentState = .PLAYING
    }
    
    private func onPause(event: PauseEvent) {
        TheoMMIntegration.logDebugStatement("Pause event occured \(event)")
        mmSmartStreaming.report(MMPlayerState.PAUSED);
    }
    
    private func onProgress(event: ProgressEvent) {
        TheoMMIntegration.logDebugStatement("Progress event occured \(event) \(event.type)")
    }
    
    private func onDurationChanged(event: DurationChangeEvent){
        TheoMMIntegration.logDebugStatement("DurationChanged event occured \(event)")
        if self.playerDuration <= 0{
            if let duration = event.duration{
                if duration != Double.nan && duration < Double.infinity{
                    playerDuration = getTimeInMs(timeInSec: duration)
                }else{
                    playerDuration = -1
                }
            }
        }
    }
    
    private func onSeeked(event: SeekedEvent){
        TheoMMIntegration.logDebugStatement("Seeked event occured \(event)")
        mmSmartStreaming.reportPlayerSeekCompleted(getTimeInMs(timeInSec: event.currentTime))
    }
    
    private func onWaiting(event: WaitingEvent){
        TheoMMIntegration.logDebugStatement("Waiting event occured \(event)")
        mmSmartStreaming.reportBufferingStarted()
    }
    
    private func getTimeInMs(timeInSec:Double) -> Int{
        let timeInMS = Int(timeInSec * 1000)
        return timeInMS
    }
    
    private func onTimeUpdate(event: TimeUpdateEvent){
        TheoMMIntegration.logDebugStatement("TimeUpdate event occured \(event)")
        mmSmartStreaming.reportPlaybackPosition(getTimeInMs(timeInSec: event.currentTime))
    }
    
    func onEnded(event: EndedEvent){
        TheoMMIntegration.logDebugStatement("Ended event occured \(event)")
        self.reportStoppedState()
    }
    
    func onRateChange(event: RateChangeEvent){
        //Good metric for microscope, wonder we should add it
        TheoMMIntegration.logDebugStatement("Rate Change event occured \(event)")
    }
    
    private func onReadyStateChange(event: ReadyStateChangeEvent){
        TheoMMIntegration.logDebugStatement("Ready State Change event occured \(event)")
    }
    
    private func onLoadedMetaData(event: LoadedMetaDataEvent){
        TheoMMIntegration.logDebugStatement("Loaded MetaData event occured \(event)")
    }
        
    private func onLoadedData(event: LoadedDataEvent){
        TheoMMIntegration.logDebugStatement("Loaded Data event occured \(event)")
    }
    
    private func onCanPlay(event: CanPlayEvent){
        TheoMMIntegration.logDebugStatement("CanPlay event occured \(event)")
        mmSmartStreaming.reportBufferingCompleted()
    }
    
    private func onCanPlayThrough(event: CanPlayThroughEvent){
        TheoMMIntegration.logDebugStatement("CanPlayThrough event occured \(event)")
    }
    
    private func onError(event: ErrorEvent){
        TheoMMIntegration.logDebugStatement("Error event occured \(event)")
        reportError(error: event.error, playbackPosMilliSec: -1)
    }
    
    private func onPresentationModeChange(event: PresentationModeChangeEvent){ //pip, inline or full screen
        TheoMMIntegration.logDebugStatement("PresentationModeChange event occured \(event)")
    }
    
    private func setPresentationSize(){
        guard let theoPlayer = self.player else{
            return
        }
        
        self.Width = Int(theoPlayer.frame.size.width)
        self.Height = Int(theoPlayer.frame.size.height)
        
        if(self.Width > 0 && self.Height > 0){
            mmSmartStreaming.reportPresentationSize(withWidth: self.Width, andHeight: self.Height)
        }
    }
    
    private func onResize(event: ResizeEvent){
        TheoMMIntegration.logDebugStatement("Resize event occured \(event)")
        setPresentationSize()
    }
    
    private func onLoadStart(event: LoadStartEvent){
        TheoMMIntegration.logDebugStatement("LoadStart event occured \(event)")
    }
    
    private func onSourceChanged(event: SourceChangeEvent){
        TheoMMIntegration.logDebugStatement("Source Changed .. \(event)")
        if let source = event.source{
            if let metadata = source.metadata{
                if let kvps = metadata.metadataKeys{
                    if let assetInfo = kvps[TheoMMIntegration.KAssetInformationKey]{
                        TheoMMIntegration.logDebugStatement("onSourceChanged assetInfo \(assetInfo)")
                        let assetStr = assetInfo as! String
                        if assetStr != "" {
                            if let assetData = assetStr.data(using: .utf8){
//                                let assetInformation = try? JSONDecoder().decode(MMAssetInformation.self, from: assetData)
//                                if let aInfo = assetInformation{
//                                    TheoMMIntegration.changeAssetForPlayer(assetInfo: aInfo)
//                                    return
//                                }
                            }
                        }
                    }
                }
            }
        }
        
        if let eventSource = event.source{
            if eventSource.sources.count > 0{
                let basicAssetInfo = MMAssetInformation(assetURL: eventSource.sources[0].src.absoluteString, assetID: TheoMMIntegration.shared.assetInfo?.assetID, assetName: TheoMMIntegration.shared.assetInfo?.assetName, videoId: TheoMMIntegration.shared.assetInfo?.videoId)
                TheoMMIntegration.changeAssetForPlayer(assetInfo: basicAssetInfo)
            }
        }
    }
    
    private func onDestroy(event: DestroyEvent){
        TheoMMIntegration.logDebugStatement("CanPlay event occured \(event)")
        cleanupInstance()
    }
    
    private func removeListeners(){
        guard let theoplayer = self.player else{
            return;
        }
        
        if (playerContentCallbacksRegd == true){
            theoplayer.removeEventListener(type: PlayerEventTypes.PLAY, listener: listeners[TheoPlayerItemPropertiesToObserve.Play.rawValue]!)
            theoplayer.removeEventListener(type: PlayerEventTypes.PLAYING, listener: listeners[TheoPlayerItemPropertiesToObserve.Play.rawValue]!)
            theoplayer.removeEventListener(type: PlayerEventTypes.PAUSE, listener: listeners[TheoPlayerItemPropertiesToObserve.Pause.rawValue]!)
            theoplayer.removeEventListener(type: PlayerEventTypes.PROGRESS, listener: listeners[TheoPlayerItemPropertiesToObserve.Progress.rawValue]!)
            theoplayer.removeEventListener(type: PlayerEventTypes.DURATION_CHANGE, listener: listeners[TheoPlayerItemPropertiesToObserve.DurationChange.rawValue]!)
            theoplayer.removeEventListener(type: PlayerEventTypes.SEEKED, listener: listeners[TheoPlayerItemPropertiesToObserve.Seeked.rawValue]!)
            theoplayer.removeEventListener(type: PlayerEventTypes.WAITING, listener: listeners[TheoPlayerItemPropertiesToObserve.Waiting.rawValue]!)
            theoplayer.removeEventListener(type: PlayerEventTypes.TIME_UPDATE, listener: listeners[TheoPlayerItemPropertiesToObserve.TimeUpdate.rawValue]!)
            theoplayer.removeEventListener(type: PlayerEventTypes.ENDED, listener: listeners[TheoPlayerItemPropertiesToObserve.Ended.rawValue]!)
            theoplayer.removeEventListener(type: PlayerEventTypes.ERROR, listener: listeners[TheoPlayerItemPropertiesToObserve.Error.rawValue]!)
            theoplayer.removeEventListener(type: PlayerEventTypes.RESIZE, listener: listeners[TheoPlayerItemPropertiesToObserve.Resize.rawValue]!)
            theoplayer.removeEventListener(type: PlayerEventTypes.DESTROY, listener: listeners[TheoPlayerItemPropertiesToObserve.Destroy.rawValue]!)
            theoplayer.removeEventListener(type: PlayerEventTypes.SOURCE_CHANGE, listener: listeners[TheoPlayerItemPropertiesToObserve.SourceChange.rawValue]!)

            theoplayer.removeEventListener(type: PlayerEventTypes.LOADED_DATA, listener: listeners[TheoPlayerItemPropertiesToObserve.LoadedData.rawValue]!)
            theoplayer.removeEventListener(type: PlayerEventTypes.RATE_CHANGE, listener: listeners[TheoPlayerItemPropertiesToObserve.RateChange.rawValue]!)
            theoplayer.removeEventListener(type: PlayerEventTypes.READY_STATE_CHANGE, listener: listeners[TheoPlayerItemPropertiesToObserve.ReadyStateChange.rawValue]!)
            theoplayer.removeEventListener(type: PlayerEventTypes.LOADED_META_DATA, listener: listeners[TheoPlayerItemPropertiesToObserve.LoadedMetaData.rawValue]!)
            theoplayer.removeEventListener(type: PlayerEventTypes.CAN_PLAY, listener: listeners[TheoPlayerItemPropertiesToObserve.CanPlay.rawValue]!)
            theoplayer.removeEventListener(type: PlayerEventTypes.CAN_PLAY_THROUGH, listener: listeners[TheoPlayerItemPropertiesToObserve.CanPlayThrough.rawValue]!)
            theoplayer.removeEventListener(type: PlayerEventTypes.PRESENTATION_MODE_CHANGE, listener: listeners[TheoPlayerItemPropertiesToObserve.PresentationModeChange.rawValue]!)
            theoplayer.removeEventListener(type: PlayerEventTypes.LOAD_START, listener: listeners[TheoPlayerItemPropertiesToObserve.LoadStart.rawValue]!)
            theoplayer.removeEventListener(type: VideoTrackListEventTypes.CHANGE, listener: listeners[TheoPlayerItemPropertiesToObserve.TrackChangeEvent.rawValue]!)
            
            self.reportStoppedState()
            playerContentCallbacksRegd = false
            let url = theoplayer.source?.sources[0].src.absoluteString
            TheoMMIntegration.logDebugStatement("resetSession for \(String(describing: url)) ***")
        }
    }
    
    private func cleanupSession(player:THEOplayer?){
        TheoMMIntegration.logDebugStatement("removeSession ***")
        removeListeners()
    }
    
    private func reportStoppedState(){
        if(currentState != .IDLE && currentState != .STOPPED){
            mmSmartStreaming.report(.STOPPED)
            currentState = .STOPPED
        }
    }
    
    private func reportUserInitiatedPlayback(){
        mmSmartStreaming.reportUserInitiatedPlayback()
    }
    
    private func reportPlayerSeekCompleted(seekEndPos:Int){
        mmSmartStreaming.reportPlayerSeekCompleted(seekEndPos)
    }
    
    private enum CurrentPlayerState{
        case IDLE, //Initial state
        PLAY, //play pressed
        PLAYING, //in playback state
        PAUSED,
        STOPPED,
        ERROR
    };
    
    lazy var mmSmartStreaming:MMSmartStreaming = MMSmartStreaming.getInstance() as! MMSmartStreaming
    private var playerContentCallbacksRegd = false
    private var presentationInfoSet = false
    private var currentState = CurrentPlayerState.IDLE
    public var assetInfo:MMAssetInformation?
    private var userIntentToPlaySignalled = false
    private static var enableLogging = false
    private static var appNotificationObsRegistered = false
    private var playerDuration = 0
    private var Width = 0
    private var Height = 0
    weak private var player:THEOplayer?
    var listeners : [String: EventListener] = [:]
}
