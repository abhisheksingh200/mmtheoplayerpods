Pod::Spec.new do |s|
  s.name             = 'MMSmartStreamingTHEOPlayer'
  s.version          = '0.0.7'
  s.summary          = 'The MediaMelon Player SDK Provides SmartSight Analytics and QBR SmartStreaming.'
  s.description      = 'The MediaMelon Player SDK adds SmartSight Analytics and QBR SmartStreaming capability to any media player and is available for all ABR media players.'
  s.homepage         = "https://bitbucket.org/abhisheksingh200/mmtheoplayerpods.git"
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Anurag Singh' => 'anurag.singh@tychotechnologies.in' }
  s.source           = { :git => "https://bitbucket.org/abhisheksingh200/mmtheoplayerpods.git", :tag => "0.0.7" }
  s.swift_version    = '4.0'
  s.ios.deployment_target = '9.0'
  s.tvos.deployment_target = '9.0'
  s.source_files = 'MMSmartStreamingTHEOPlayer/Classes/Common/*.{h,swift}', 'MMSmartStreamingTHEOPlayer/Classes/Wrapper/TheoPlayer/*.swift'
  s.frameworks = 'UIKit', 'AVFoundation'
  s.ios.frameworks = 'CoreTelephony'
  s.ios.vendored_libraries = 'MMSmartStreamingTHEOPlayer/Classes/StaticLibrary/iOS/libmmsmartstreamer.a'
  s.tvos.vendored_libraries = 'MMSmartStreamingTHEOPlayer/Classes/StaticLibrary/tvOS/libmmsmartstreaming-tvos.a'
  s.libraries = 'stdc++'
  s.public_header_files = 'MMSmartStreamingTHEOPlayer/Classes/Common/*.h'
  s.pod_target_xcconfig = {'VALID_ARCHS[sdk=iphonesimulator*]' => 'armv7 arm64 x86_64', 'VALID_ARCHS[sdk=appletvsimulator*]' => ''}
  s.ios.vendored_frameworks = 'MMSmartStreamingTHEOPlayer/Frameworks/THEOplayer/iOS/THEOplayerSDK.framework'
  s.tvos.vendored_frameworks = 'MMSmartStreamingTHEOPlayer/Frameworks/THEOplayer/tvOS/THEOplayerSDK.framework'
  
end
