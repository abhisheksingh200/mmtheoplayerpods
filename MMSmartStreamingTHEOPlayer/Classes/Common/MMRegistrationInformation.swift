//
//  MMRegistrationInformation.swift
//  AllInOneDemoPlayer
//
//  Created by MacAir 1 on 08/10/20.
//  Copyright © 2020 FreeWheel. All rights reserved.
//

import UIKit
import Foundation

@objc public class MMRegistrationInformation: NSObject {
    //MARK:- OBJECTS
    private let kEXTERNALSUBSCRIBERIDADDEDDATE = "EXTERNAL-SUBSCRIBER-ID-ADDEDDATE"
    private let kEXTERNALSUBSCRIBERID = "EXTERNAL-SUBSCRIBER-ID"
    public var customerID: String!
    public var component: String!
    public var playerName: String!
    public var domainName: String?
    public var subscriberID: String?
    public var subscriberType: String?
    public var subscriberTag: String?
    public var playerBrand: String?
    public var playerModel: String?
    public var playerVersion: String?
    
    //MARK:- METHODS
    /*
     * Creates the object to have information identifying the customer, subscriber, and player to which integration is done
     */
    @objc public init(customerID cID: String, playerName pName: String) {
        self.customerID = cID
        self.playerName = pName
    }

    /*
     * Some business organizations may would like to do analytics segmented by group.
     * For example, a Media House may have many divisions, and will like to categorize their analysis
     * based on division. Or a content owner has distributed content to various resellers and would
     * like to know the reseller from whom the user is playing the content.
     * In this case every reseller will have separate application, and will configure the domain name.
     *
     * Call to this API is optional
     */
    @objc public func setDomain(_ dName: String?) {
        if let dName = dName {
            self.domainName = dName
        }
    }
    
    /*
     * Provides the subscriber information to the SDK.
     * Subscriber information includes identifier identifying the subscriber (genrally email id, or UUID of app installation etc.),
     * Its type - For example Premium, Basic etc (Integrators can choose any value for type depending on the damain of business in
     * which player is used. From perspective of Smartsight, it is opaque data, and is not interpreted in any way by it.
     * Tag - Additional metadata corresponding to the asset. From perspective of Smartsight, no meaning is attached to it, and it is
     * reflect as is.
     *
     * Call to this API is optional
     */
    @objc public func setSubscriberInformation(subscriberID subsID: String?, subscriberType subsType: String?, subscriberTag subsTag: String?) {
        var isUUIDNeeded = false
        if subsID == nil {
            isUUIDNeeded = true
        } else if subsID!.isEmpty {
            isUUIDNeeded = true
        }
        
        if let subsID = subsID {
            self.subscriberID = subsID
        }
        
        if isUUIDNeeded {
            if UserDefaults.standard.value(forKey: self.kEXTERNALSUBSCRIBERIDADDEDDATE) != nil {
                let externalSubscriberIDAddedDate = UserDefaults.standard.value(forKey: self.kEXTERNALSUBSCRIBERIDADDEDDATE) as! Date
                let today = Date()
                let dayDifference = today.interval(ofComponent: .day, fromDate: externalSubscriberIDAddedDate)
                let lastSetExternalSubscriberID = UserDefaults.standard.value(forKey: self.kEXTERNALSUBSCRIBERID) as? String
                
                if (dayDifference <= 30 && lastSetExternalSubscriberID != nil) {
                    self.subscriberID = lastSetExternalSubscriberID
                } else {
                    self.subscriberID = UUID().uuidString
                    UserDefaults.standard.set(self.subscriberID, forKey: self.kEXTERNALSUBSCRIBERID)
                    UserDefaults.standard.set(Date(), forKey: self.kEXTERNALSUBSCRIBERIDADDEDDATE)
                    UserDefaults.standard.synchronize()
                }
            } else {
                self.subscriberID = UUID().uuidString
                UserDefaults.standard.set(self.subscriberID, forKey: self.kEXTERNALSUBSCRIBERID)
                UserDefaults.standard.set(Date(), forKey: self.kEXTERNALSUBSCRIBERIDADDEDDATE)
                UserDefaults.standard.synchronize()
            }
        }
        
        if let subsType = subsType {
            self.subscriberType = subsType
        }
        
        if let subsTag = subsTag {
            self.subscriberTag = subsTag
        }
    }
    
    /**
     * Sets the player information. Please note that brand, model and version mentioned here are with respect to player and not wrt device
     * i.e. Even though brand for device is Apple, but brand here could be the brand, that integrator want to assign to this player.
     * For example - It could be the name of Media Vendor.
     * Model - This could be used to further classify the player, for example XYZ framework based player, or VOD player or Live player etc
     * Version - This is used to indicate the version of the player
     * All these params are optionals and you may set them to nil
     *
     * Call to this API is optional
     */
    @objc public func setPlayerInformation(brand: String?, model: String?, version: String?){
        if let brand = brand {
            self.playerBrand = brand
        }
        
        if let model = model {
            self.playerModel = model
        }
        
        if let version = version {
            self.playerVersion = version
        }
    }
    
    @objc public func setComponentName(_ cName: String?) {
        if let compName = cName {
            self.component = compName
        }
    }
}

//MARK:- MM OVERRIDABLE METRICS
public enum MMOverridableMetrics{
    /**
     * Latency - Time between when user requests the start of the playback session and playback starts.
     */
    case Latency,
    
    /**
     * CDN - IP address of manifest server
     */
    CDN,
    
    /**
     * DurationWatched - Duration of time that the player was in the PLAYING state, excluding advertisement play time.
     */
    DurationWatched
}

//MARK:- DATE EXTENSION TO FIND THE INTERVAL BETWEEN 2 DATES
extension Date {
    func interval(ofComponent comp: Calendar.Component, fromDate date: Date) -> Int {
        let currentCalendar = Calendar.current
        guard let start = currentCalendar.ordinality(of: comp, in: .era, for: date) else { return 0 }
        guard let end = currentCalendar.ordinality(of: comp, in: .era, for: self) else { return 0 }
        return end - start
    }
}
